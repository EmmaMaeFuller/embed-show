import { Meta, Title, useParams, useRouteData } from "solid-start";
import { createServerData$ } from "solid-start/server";
import { createClient } from "@supabase/supabase-js";
import { SUPABASE_ANON_KEY, SUPABASE_URI } from "~/db";
import { Database } from "~/db/schema";
import { createEffect } from "solid-js";

export const routeData = () =>
  createServerData$(async () => {
    let { data } = await createClient<Database>(
      SUPABASE_URI,
      SUPABASE_ANON_KEY
    )
      .from("redirects")
      .select()
      .eq("id", useParams().id)
      .single();

    return data;
  });

export default function Home() {
  const data = useRouteData<typeof routeData>();

  createEffect(() => {
    if (data()) location.href = data()!.redirect_to;
  });

  return (
    <main>
      <Title>{data()?.title}</Title>
      <Meta name="description" content={data()?.description} />

      <Meta property="og:url" content="show.embed" />
      <Meta property="og:type" content="website" />
      <Meta property="og:title" content={data()?.title} />
      <Meta property="og:description" content={data()?.description} />
      <Meta property="og:image" content={data()?.image_url} />

      <Meta name="twitter:card" content="summary_large_image" />
      <Meta property="twitter:domain" content="show.embed" />
      <Meta name="twitter:title" content={data()?.title} />
      <Meta name="twitter:description" content={data()?.description} />
      <Meta name="twitter:image" content={data()?.image_url} />
    </main>
  );
}
