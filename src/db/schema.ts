export type Json =
  | string
  | number
  | boolean
  | null
  | { [key: string]: Json }
  | Json[];

export interface Database {
  public: {
    Tables: {
      redirects: {
        Row: {
          id: string;
          created_at: string;
          title: string;
          description: string;
          image_url: string;
          redirect_to: string;
        };
        Insert: {
          id: string;
          created_at?: string;
          title?: string;
          description?: string;
          image_url?: string;
          redirect_to?: string;
        };
        Update: {
          id?: string;
          created_at?: string;
          title?: string;
          description?: string;
          image_url?: string;
          redirect_to?: string;
        };
      };
    };
    Views: {
      [_ in never]: never;
    };
    Functions: {
      [_ in never]: never;
    };
    Enums: {
      [_ in never]: never;
    };
  };
}
